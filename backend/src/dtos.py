import json

class MovieDTO():
    def __init__(self, movie): 
        self.movie = movie
    def serialize(self):
        return {
            "id": self.movie.movieId,
            "title": self.movie.title,
            "category": self.movie.category,
            "actors": self.movie.actor.split(";"),
            "price": self.movie.price,
        }

