from . import db

class Movie(db.Model):
    __tablename__ = "movies"
    def __init__(self, title, category, actor, price): 
        self.title = title
        self.category = category
        self.actor = actor
        self.price = price
    
    movieId = db.Column('movie_id', db.Integer, primary_key=True)
    title = db.Column(db.String(200), nullable=False, unique= True)
    category = db.Column(db.String(100), nullable=False)
    actor = db.Column(db.String(1000), nullable=False)
    price = db.Column(db.Float, nullable=False) 